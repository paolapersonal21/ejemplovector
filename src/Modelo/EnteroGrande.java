/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 * Está clase modela un entero largo a través de un vector de enteros
 *
 * @author Marco Adarme
 */
public class EnteroGrande {

    private int numeros[];

    /**
     * Constructor vacío
     */
    public EnteroGrande() {
    }

    /**
     * Crea un vector a través del string. La cadena viene dividida por ";"
     * Ejemplo: valores="12;34;45" Entonces vector= 12- 34-45
     *
     * @param valores un vector de string que contiene los valores del vector
     */
    public EnteroGrande(String valores) {
        String cadena2[] = valores.split(";");
        this.numeros = new int[cadena2.length];
        //Transladar de cadena2 a numeros, convirtiendo
        for (int i = 0; i < cadena2.length; i++) {
            int dato = Integer.parseInt(cadena2[i]);
            this.numeros[i] = dato;

        }
    }

    /**
     * Método que retorna en una cadena el vector
     *
     * @return un String
     */
    public String toString() {
        String cadena = "";
        for (int dato : this.numeros) {
            cadena += dato + "\t";
        }
        return cadena;
    }

    /**
     * Método de la burbuja, tomado de:
     * https://parzibyte.me/blog/2019/12/26/ordenamiento-burbuja-java/#google_vignette
     */
    public void sort() {
        
        int arreglo[]=this.numeros;
        for (int x = 0; x < arreglo.length; x++) {
            // Aquí "y" se detiene antes de llegar
            // a length - 1 porque dentro del for, accedemos
            // al siguiente elemento con el índice actual + 1
            for (int y = 0; y < arreglo.length - 1; y++) {
                int elementoActual = arreglo[y],
                        elementoSiguiente = arreglo[y + 1];
                if (elementoActual > elementoSiguiente) {
                    // Intercambiar
                    arreglo[y] = elementoSiguiente;
                    arreglo[y + 1] = elementoActual;
                }
            }
        }
    }

}
